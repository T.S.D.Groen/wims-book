
# Elektromagnetische inductie

In dit hoofdstuk bestuderen we verder de interactie tussen elektrische en magnetische velden.
Dit is de basis van elektronische schakelingen en elektromagnetische velden waar we in volgende hoofdstukken verder op in gaan.

## Wet van Faraday

<iframe
    width="600"
    height="300"
    src="https://youtube.com/embed/HUFBdnqPGKE?align=center"
    frameborder="0"
    allowfullscreen
></iframe>

De wet van Faraday beschrijft kwantitatief hoe een verandering in magnetische flux een inductiespanning opwekt.
In deze video leggen we de begrippen hierbij uit.

<iframe
    width="600"
    height="300"
    src="https://www.youtube.com/embed/cYLQ2TK0eyU?align=center"
    frameborder="0"
    allowfullscreen
></iframe>

In dit voorbeeld laten we zien hoe een veranderend oppervlak van een lus in een magnetisch veld een inductiestroom opwekt.

<iframe
    width="600"
    height="300"
    src="https://www.youtube.com/embed/IdFTPLpvvto?align=center"
    frameborder="0"
    allowfullscreen
></iframe>

De wet van Lenz maakt duidelijk in welke richting de inductiestroom gaat lopen. 
Dit is heel belangrijk voor het begrip van systemen met inductiespanningen.

<iframe
    width="600"
    height="300"
    src="https://youtube.com/embed/6hnnuk1GelU?align=center"
    frameborder="0"
    allowfullscreen
></iframe>

Kwalitatief is het nu al mogelijk om veel voorbeelden en toepassingen van inductiestroom te begrijpen, zoals
verstoringen door zonnestormen, contactloze remmen, metaaldetectoren, dynamo's en transformatoren.

## Spoelen schakelen

<iframe
    width="600"
    height="300"
    src="https://youtube.com/embed/CDje_TlUXVE?align=center"
    frameborder="0"
    allowfullscreen
></iframe>

Een stroom door een spoel genereert een magnetisch veld, wat door de spoel zelf weer wordt opgevangen.
Dat leidt volgens de wet van Faraday tot een tegengestelde inductiespanning.
Dit begrip wordt zelf-inductie genoemd en is belangrijk in schakelingen.
In de video wordt de definitie gegeven en uitgerekend voor een spoel in de *spin-echo small angle neutron scattering instrument*.

<iframe
    width="600"
    height="300"
    src="https://www.youtube.com/embed/CijwaleQPjU?align=center"
    frameborder="0"
    allowfullscreen
></iframe>

Het plotseling uitschakelen van de stroom door een spoel kan daarom tot een grote spanningssprong leiden die gevaarlijk voor een instrument kan zijn. 
In de video rekenen we uit hoe groot deze kan zijn.

<iframe
    width="600"
    height="300"
    src="https://youtube.com/embed/aplUIW9gka0?align=center"
    frameborder="0"
    allowfullscreen
></iframe>

Bij het beschouwen van inschakeleffecten bij spoelen is het handig om te kijken naar de korte en lange tijdslimieten in de stroom die wordt door gelaten. 
Zonder te rekenen weet je dan al wat er voor stromen gaan lopen door een schakeling.

<iframe
    width="600"
    height="300"
    src="https://www.youtube.com/embed/gZn_DYhxJBc?align=center"
    frameborder="0"
    allowfullscreen
></iframe>

Geheel analoog aan de afleiding voor het tijdsgedrag van condensatoren, is het mogelijk om analytische het tijdsgedrag van spoelen te beschrijven. 
Dit leidt weer tot exponentiele functies.

## Energie in spoelen en magnetische velden

<iframe
    width="600"
    height="300"
    src="https://www.youtube.com/embed/XPvyjlYzcjQ?align=center"
    frameborder="0"
    allowfullscreen
></iframe>

Het opladen van een spoel kost energie en die energie gaat blijkbaar in de spoel zitten.
Dat moet dus wel in het magnetische veld zijn. Dit is een manier om energie tijdelijk op te slaan.

<iframe
    width="600"
    height="300"
    src="https://www.youtube.com/embed/HKgeePRQchc?align=center"
    frameborder="0"
    allowfullscreen
></iframe>

Dit maakt het heel aannemelijk dat er ook in analogie met elektronische velden, energie is in magnetische velden.
Dat reken we in deze video uit.

## Inductievelden zijn niet conservatief
<iframe
    width="600"
    height="300"
    src="https://www.youtube.com/embed/lPP2mFwOlDs?align=center"
    frameborder="0"
    allowfullscreen
></iframe>

De wet van Faraday kan ook geformuleerd worden als het induceren van een elektrisch veld.
Dit is een fundamentelere manier van schrijven, die het duidelijk maakt dat deze velden niet conservatief zijn, in tegenstelling tot wat we in de eerste hoofdstukken van de elektrostatica hebben gezien.

<iframe
    width="600"
    height="300"
    src="https://www.youtube.com/embed/kxa0PThfPSg?align=center"
    frameborder="0"
    allowfullscreen
></iframe>

Als voorbeeld berekenen we het geinduceerde veld in een spoel waarin de stroom continue toeneemt.

## Diamagnetisme

<iframe
    width="600"
    height="300"
    src="https://www.youtube.com/embed/LzFDgBlr0EI?align=center"
    frameborder="0"
    allowfullscreen
></iframe>

Verschillende materialen kunnen verschillende magnetische interacties hebben.
Diamagnetische materialen laten geen magneetvelden door dringen.
Dit verklaart levitatie in hoge magneetvelden.

<iframe
    width="600"
    height="300"
    src="https://www.youtube.com/embed/H3GuLepOKFY?align=center"
    frameborder="0"
    allowfullscreen
></iframe>

Deze demonstratie met een supergeleider en vloeibaar stikstof laat levitatie door magnetisme zien.

## Samenvattingen magnetisme

<iframe
    width="600"
    height="300"
    src="https://www.youtube.com/embed/7I-mbv40oxY?align=center"
    frameborder="0"
    allowfullscreen
></iframe>

In deze samenvatting laten we zien hoe magneetvelden veroorzaakt worden door bewegende ladingen.
We beschouwen de analogie tussen elektrische en magnetische velden via de wetten van Maxwell die we later nog verder gaan verdiepen.

<iframe
    width="600"
    height="300"
    src="https://www.youtube.com/embed/W77nRtc5qSg?align=center"
    frameborder="0"
    allowfullscreen
></iframe>

In deze samenvatting bekijken we de analogie tussen condensatoren en spoelen en tussen de energiedichtheid in elektrische en magnetische velden.
