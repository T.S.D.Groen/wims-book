
# Condensatoren
In het algemeen is er elektrostatische energie in een configuratie van elektrische ladingen. 
Dit principe wordt gebruikt in condensatoren (Engels: capacitors), die elektrische lading kunnen opslaan en dus ook energie.
Er zijn veel toepassingen in de elektronica, die in de komende hoofdstukken verder worden uitgewerkt.
In dit hoofdstuk onderzoeken we vooral de lading en energie die er in condensatoren worden opgeslagen.

## Elektrostatische energie en condensatoren

<iframe
    width="600"
    height="300"
    src="https://youtube.com/embed/zt_x9bae0gI?align=center"
    frameborder="0"
    allowfullscreen
></iframe>

Een configuratie van ladingen in de ruimte heeft een zekere elektrostatische energie.
Een duidelijke configuratie is te maken met een platte plaatcondensator. 
Hierbij kan je een capaciteit defini&euml;ren, dat wil zeggen de hoeveelheid lading per aangelegde potentiaal.
We berekenen hoeveel energie er in een opgeladen condensator wordt opgeslagen.

<iframe
    width="600"
    height="300"
    src="https://www.youtube.com/embed/bdADs7cjhEc?align=center"
    frameborder="0"
    allowfullscreen
></iframe>

Op dezelfde manier kun je chemische reacties zo natuurkundig beschouwen als herrangschikking van elektrische ladingen.

<iframe
    width="600"
    height="300"
    src="https://www.youtube.com/embed/GBh4EB49bt0?align=center"
    frameborder="0"
    allowfullscreen
></iframe>

Een condensator kan ook andere geometri&euml;n hebben, bijvoorbeeld een cilinder. 
Aan de hand van de definitie van de capaciteit is deze uit te rekenen.

## Di&euml;lektricum

<iframe
    width="600"
    height="300"
    src="https://www.youtube.com/embed/GIHKbIwB96E?align=center"
    frameborder="0"
    allowfullscreen
></iframe>

Door een di&euml;lektricum in een condensator te plaatsen, kan je de capaciteit flink verhogen. 
Dipolen in het di&euml;lektricum werken het elektrisch veld tegen, zodat je bij een zekere lading een veel lagere potentiaal hebt.

## Schakelingen

<iframe
    width="600"
    height="300"
    src="https://www.youtube.com/embed/C4T234rc2wg?align=center"
    frameborder="0"
    allowfullscreen
></iframe>

Condensatoren kan je in serie of parallel schakelen, zodat je effectief een condensator krijgt met een andere capaciteit.
De vervangingswaarde van de capaciteit van een schakeling kan je goed uitrekenen als je de natuurkunde erachter begrijpt.

<iframe
    width="600"
    height="300"
    src="https://youtube.com/embed/So5Oc9f9b6c?align=center"
    frameborder="0"
    allowfullscreen
></iframe>

Met deze duidelijke <a href="https://phet.colorado.edu/nl/simulations/electric-hockey" target="_blank">physlet</a> kan je een goed fysisch beeld krijgen van de effecten van oppervlakte en afstand op de capaciteit van een condensator. 
Ook kan je het effect van een di&euml;lektricum zichtbaar maken.
Er zijn ook meerdere schakelingen in verwerkt.

## Energie van een elektrisch veld

<iframe
    width="600"
    height="300"
    src="https://www.youtube.com/embed/mwsTjJPY9n4?align=center"
    frameborder="0"
    allowfullscreen
></iframe>

In een condensator is energie opgeslagen en er is een elektrisch veld.
Voor een platte plaat-condensator kan je zo de energiedichtheid van het elektrische veld uitrekenen.
Deze relatie is veel algemener toepasbaar.
