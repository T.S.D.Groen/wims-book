
# Elektrische stroom
Tot nu toe hebben we alleen ladingen in een elektrostatisch evenwicht bestudeerd.
Nu gaan we bewegende ladingen bestuderen, zowel macroscopisch als microscopisch en het verband hiertussen.

## Definitie stroom

<iframe
    width="600"
    height="300"
    src="https://youtube.com/embed/byyiBcNmadY?align=center"
    frameborder="0"
    allowfullscreen
></iframe>

De stroom is gedefini&euml;erd als de hoeveelheid lading die per tijdseenheid ergens doorheen stroomt.

## Microscopisch beeld van stroom, geleiding en weerstand

<iframe
    width="600"
    height="300"
    src="https://www.youtube.com/embed/2F0NqWghmJA?align=center"
    frameborder="0"
    allowfullscreen
></iframe>

Stroom kan je goed microscopisch beschrijven in termen van elementaire ladingsdragers, driftsnelheid, resistiviteit en stroomdichtheid.
Al deze parameters zijn dan ook weer om te zetten naar de macroscopische equivalente parameters.

<iframe
    width="600"
    height="300"
    src="https://youtube.com/embed/coMCJf0kPSU?align=center"
    frameborder="0"
    allowfullscreen
></iframe>

De <a href="https://phet.colorado.edu/sims/html/resistance-in-a-wire/latest/resistance-in-a-wire_en.html" target="_blank">physlet</a> geeft een duidelijk beeld hoe de macroscopische weerstand gerelateerd is aan de microscopische resistiviteit.

## Geleidingsmechanismes

<iframe
    width="600"
    height="300"
    src="https://www.youtube.com/embed/1MgEXRzGvsc?align=center"
    frameborder="0"
    allowfullscreen
></iframe>

Er zijn meerdere mechanismes voor geleiding met allemaal andere eigenschappen. 
Wat ze gemeenschappelijk hebben is dat er mobiele ladingsdragers zijn.
Bij ieder mechanisme zijn er weer heel andere resistiviteiten.

<iframe
    width="600"
    height="300"
    src="https://www.youtube.com/embed/riR-e0m83Q8?align=center"
    frameborder="0"
    allowfullscreen
></iframe>

In deze demonstratie aan de hand temperatuursafhankelijkheid van de weerstand laten we zien dat verschillende materialen verschillende geleidingsmechanismes hebben.

## Schakelingen met weerstanden

<iframe
    width="600"
    height="300"
    src="https://youtube.com/embed/El25cMbKvOg?align=center"
    frameborder="0"
    allowfullscreen
></iframe>

Je kan weerstanden op verschillende manieren in schakelingen verwerken: in serie, parallel of combinaties hiervan. Je krijgt meestal dan een situatie waarbij je een combinatie van weerstanden zou kunnen vervangen door eentje met een andere weerstand.

## Vermogen

<iframe
    width="600"
    height="300"
    src="https://www.youtube.com/embed/r9XRyTUgoO8?align=center"
    frameborder="0"
    allowfullscreen
></iframe>

Er is vermogen nodig om lading door een weerstand te laten stromen. Let er goed op of de stroom of de spanning bepaald is als je er aan gaat rekenen.
