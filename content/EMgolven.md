
# Elektromagnetische golven


<iframe
    width="600"
    height="300"
    src="https://www.youtube.com/embed/ctFpKhp_C2g?align=center"
    frameborder="0"
    allowfullscreen
></iframe>

Er lijkt bijna symmetrie te zijn in de wetten van Maxwell voor elektriciteit en magnetisme, maar er ontbreekt nog een term in de wet van Ampere. 
In deze video laten we zien hoe er met een vervangingsstroom in de wet van Ampere meer symmetrie te krijgen is.

<iframe
    width="600"
    height="300"
    src="https://www.youtube.com/embed/gUa2dn59vnI?align=center"
    frameborder="0"
    allowfullscreen
></iframe>

Elektromagnetische golven blijken te voldoen aan de 4 wetten van Maxwell, dus ze kunnen bestaan.
We beginnen met een korte opfrisser over lopende golven.

<iframe
    width="600"
    height="300"
    src="https://youtube.com/embed/XpuQoqsgDO0?align=center"
    frameborder="0"
    allowfullscreen
></iframe>

Elektromagnetische golven komen overal voor.
In deze video beschrijven we veel van de eigenschappen van deze golven, zoals energiestroom, polarisatie en spectrum.

<iframe
    width="600"
    height="300"
    src="https://www.youtube.com/embed/oRYF2pEUDh4?align=center"
    frameborder="0"
    allowfullscreen
></iframe>

Deze video geeft een korte samenvatting van alles wat we over elektromagnetische golven hebben geleerd.
